// Task-1 ////////////////////////////////////////////////////////////




class Person {
    constructor(name,age){
        this.name=name;
        this.age=age;
    }
}

// const person =new Person("Mak",26 )
// console.log(person);

class Baby extends Person {
    constructor(name,age,favoriteToy){
        super(name,age);
        this.favoriteToy=favoriteToy;
    }
    play(){
        return (`${this.name} X being the favorite toy.`)
    }
}

const baby = new Baby("Eshnar",5,"toy");
// console.log(baby.play());



//Task-2 /////////////////////////////////////////////////////////////////////////////

class Persons {
    constructor(name,age){
        this.name=name;
        this.age=age;
    }
    describe(){
        return `${this.name}, ${this.age} years old`   
    }
}

const jack = new Persons("Jack",25);
const jill = new Persons("Jill",24);
console.log(jack.describe());
console.log(jill.describe());













